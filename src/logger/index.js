const debug = require('debug')('test-cases:logger');
const debug_ = require('debug')('test-cases:logger_');

const log = (msg) => {
    debug(msg);
};

const error = (err) => {
    let msg = err;
    if (err instanceof Error) {
        msg = err.message;
    }
    debug_(msg);
}

module.exports = { log, error };
