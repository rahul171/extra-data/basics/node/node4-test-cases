const debug = require('debug')('test-cases');
const debug_ = require('debug')('test-cases_');
const logger = require('./logger');
const connection = require('./db');
const express = require('express');
const app = express();

connection.connect();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use((req, res, next) => {
    logger.log(`${req.method} ${req.url}`);
    next();
});

app.get('/', (req, res, next) => {
    res.status(200).send('hello');
});

const routes = require('./routes');
routes.registerRoutes(app);

app.use((err, req, res, next) => {
    logger.error(err);
    res.status(400).send(`Error occurred: ${err.message}`);
});

module.exports = app;
