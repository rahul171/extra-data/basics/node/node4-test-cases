const mongoose = require('mongoose');
const logger = require('../logger');

const connect = () => {
    return mongoose.connect(process.env.MONGODB_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    })
        .then(client => {
            logger.log('connected to the database');
            return client;
        })
        .catch(err => {
            logger.error(err);
        });
};

module.exports = { connect };
