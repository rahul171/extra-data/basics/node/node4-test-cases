const { Schema } = require('mongoose');

const schema = new Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    userId: {
        type: Schema.Types.ObjectID,
        ref: 'User'
    }
});

module.exports = schema;
