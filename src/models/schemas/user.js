const { Schema } = require('mongoose');

const schema = new Schema({
    name: {
        type: String,
        required: true,
        validate(value) {
            if (value.includes('abcd')) {
                throw new Error(`Name can't contain "abcd"`);
            }
        }
    },
    planet: {
        type: String,
        default: 'Earth'
    }
});

schema.virtual('thing', {
    ref: 'Thing',
    localField: '_id',
    foreignField: 'userId'
});

module.exports = schema;
