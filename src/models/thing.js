const mongoose = require('mongoose');
const schema = require('./schemas/thing');

const Model = mongoose.model('Thing', schema);

module.exports = Model;
