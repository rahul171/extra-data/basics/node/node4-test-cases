const others = require('../others');

test('sample', () => {
    expect(1).toEqual(1);
    expect({}).toEqual({});
    expect(null).toBeNull();
    expect(1).not.toBeNull();
});

test('async test with done', (done) => {
    setTimeout(() => {
        expect(1).toEqual(1);
        done();
    }, 0);
});

test('async with async await', async () => {
    const value = await others.addAsync(1, 2);
    expect(value).toEqual(3);
    expect(value).not.toEqual(4);
});

test('swap key value', () => {
    const obj = {
        a: 'b',
        haha: 1
    };
    const obj1 = others.swapKeyValue(obj);
    expect(obj1).toEqual({
        b: 'a',
        1: 'haha'
    });
});

test('get only first n', () => {
    const obj = {
        first: 'hello',
        a: 'b',
        c: 'd',
        someone: 'something'
    };
    const obj1 = others.returnN(obj, 2);
    expect(obj1).toEqual({
        first: 'hello',
        a: 'b'
    });
});
