const { test, expect, beforeEach, beforeAll } = require('@jest/globals');

const request = require('supertest');
const app = require('../app');

const setup = require('./fixtures/db');

const User = require('../models/user');

const abcd = require('abcd');

beforeAll(setup.connectDB);
beforeEach(setup.setupDB);

test('get user', async () => {
    const user = (await User.findById(setup.users[0]._id)).toObject();
    expect(user).toMatchObject(setup.users[0]);
});

test('get all user', async () => {
    const users = await User.find();
    expect(users).toBeInstanceOf(Array);
    expect(users).toEqual(expect.any(Array));
    // expect(users).toBe(expect.any(Array));
});

test('create user', async () => {
    const response = await request(app)
        .post('/user')
        .send({
            name: 'rahul',
            planet: 'mars'
        })
        .expect(200);

    const responseUser = response.body;

    const user = await User.findById(responseUser._id);
    expect(user).not.toBeNull();
    expect(user).toMatchObject({
        name: 'rahul',
        planet: 'mars'
    });
});

test('app get all users', async () => {
    await request(app)
        .get('/user')
        .send()
        .expect(200);
});

test('test mocks', () => {
    expect(abcd.hello()).toBe(2);
});
