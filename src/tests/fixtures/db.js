const connection = require('../../db');
const User = require('../../models/user');
const mongoose = require('mongoose');

const users = [
    {
        _id: new mongoose.Types.ObjectId(),
        name: 'rahul',
        planet: 'hello there'
    },
    {
        _id: new mongoose.Types.ObjectId(),
        name: 'test',
        planet: 'hola'
    }
];

const connectDB = async () => { await connection.connect(); };

const setupDB = async () => {
    await User.deleteMany();
    for (const user of users) {
        await new User(user).save();
    }
};

module.exports = { setupDB, users, connectDB };
