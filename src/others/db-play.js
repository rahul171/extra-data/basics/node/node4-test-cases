const User = require('../models/user');
const connection = require('../db');

const dotenv = require('dotenv');
dotenv.config({ path: '../config/test.env' });

// debugger;

(async () => {

    await connection.connect();

    const user = (await User.findById('5ed51bbf9460a27322607377')).toObject();

    console.log(user);

    // debugger;

})();
