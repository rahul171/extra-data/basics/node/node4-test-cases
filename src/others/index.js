const add = (a, b) => a + b;
const timeout = 1000;

const addAsync = async (a, b) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(add(a, b));
        }, timeout);
    });
}

const multiply = (a, b) => a * b;

const multiplyAsync = (a, b) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(multiply(a, b));
        }, timeout);
    });
}

const join = (a, b) => Number(`${a}${b}`);

const joinAsync = (a, b) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(join(a, b));
        }, timeout);
    });
}

const failAsync = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            reject('this is failed promise');
        }, timeout);
    });
}

const swapKeyValue = (obj) => {
    const out = {};
    const keys = Object.keys(obj);
    for (const key of keys) {
        out[obj[key]] = key;
    }
    return out;
}

const returnN = (obj, n) => {
    const out = {};
    const keys = Object.keys(obj);
    for (let i = 0; i < n; i++) {
        out[keys[i]] = obj[keys[i]];
    }
    return out;
}

module.exports = { add, multiply, join, addAsync, multiplyAsync, joinAsync, failAsync, swapKeyValue, returnN };
