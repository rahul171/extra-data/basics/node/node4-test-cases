const userRoutes = require('./user');
const thingRoutes = require('./thing');

const registerRoutes = (app) => {
    app.use('/user', userRoutes);
    app.use('/thing', thingRoutes);
};

module.exports = { registerRoutes };
