const router = require('express').Router();
const User = require('../models/user');
const logger = require('../logger');

router.get('/', async (req, res, next) => {
    res.status(200).send(await User.find());
});

router.get('/:id', async (req, res, next) => {
    const userId = req.params.id;
    const user = await User.findById(userId);
    if (!user) {
        return next(new Error(`User with id: ${userId} not found`));
    }
    res.status(200).send(user);
});

router.post('/', async (req, res, next) => {
    const user = new User({ ...req.body });
    try {
        await user.save();
    } catch(err) {
        return next(err);
    }
    res.status(200).send(user);
});

module.exports = router;
