const router = require('express').Router();
const Thing = require('../models/thing');

router.get('/', async (req, res, next) => {
    res.status(200).send(await Thing.find());
});

router.get('/:id', async (req, res, next) => {
    const thingId = req.params.id;
    const thing = await Thing.findById(thingId).populate('userId');
    if (!thingId) {
        return next(new Error(`Thing with id: ${thingId} not found`));
    }
    res.status(200).send(thing);
});

router.post('/', async (req, res, next) => {
    const thing = new Thing({ ...req.body });
    try {
        await thing.save();
    } catch(err) {
        return next(err);
    }
    res.status(200).send(thing);
});

module.exports = router;
