const app = require('./src/app');
const logger = require('./src/logger');

const port = process.env.PORT;

app.listen(port, () => {
    logger.log(`listening on port ${port}`);
});
